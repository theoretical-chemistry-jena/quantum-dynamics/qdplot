{ requireFile, buildPythonPackage, lib, nix-gitignore
# Python dependencies
, numpy
, matplotlib
, h5py
, opt-einsum
, eqd
, additionalDevDeps ? [ ]
}:

buildPythonPackage rec {
    pname = "qdplot";
    version = "0.1.0";

    nativeBuildInputs = additionalDevDeps;

    propagatedBuildInputs = [
      numpy
      matplotlib
      h5py
      eqd
    ];

    src = nix-gitignore.gitignoreSource [] ./..;

    meta = with lib; {
      description = "Python utilities for plotting quantum dynamics data.";
      license = licenses.mit;
      homepage = "https://gitlab.com/FabianGD/qdplot";
      platforms = platforms.unix;
    };

    doCheck = false;
}
