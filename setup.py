# -*- coding: utf-8 -*-
"""Setup file for pyQD, the split-operator TDSE package."""


from setuptools import setup


NAME = "qdplot"
DESCRIPTION = (
    "Prospect Python/Sympy implementation of an electronic quantum dynamics integrator, "
    "based on the wavefunction split-operator method."
)
URL = "https://git.rz.uni-jena.de/FabianGD/eQD"
EMAIL = "fabian.droege@uni-jena.de"
AUTHOR = "Fabian G. Dröge"
REQUIRES_PYTHON = ">=3.7.0"
REQUIRED = [
    "numpy",
    "h5py",
    "tqdm",
    "matplotlib",
    "eQD",
]
ENTRYPOINTS = {}
CLASSIFIERS = [
    # Trove classifiers
    # Full list: https://pypi.python.org/pypi?%3Aaction=list_classifiers
    "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
    "Programming Language :: Python",
    "Programming Language :: Python :: 3",
    "Programming Language :: Python :: 3.7",
]
VERSION = "0.0.1"

setup(
    name=NAME,
    description=DESCRIPTION,
    author=AUTHOR,
    author_email=EMAIL,
    url=URL,
    install_requires=REQUIRED,
    python_requires=REQUIRES_PYTHON,
    packages=[NAME],
    entry_points=ENTRYPOINTS,
    classifiers=CLASSIFIERS,
    zip_safe=False,
    license="GPLv3",
    package_data={"": ["scripts/*.py"]},
    version=VERSION,
)
