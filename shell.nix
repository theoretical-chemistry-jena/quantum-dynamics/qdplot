let
  sources = import ./nix/sources.nix { };
  pkgs = import sources.nixpkgs { config = { allowUnfree = true; }; };

  eqd = with pkgs; import sources.eqd { };
  qdplot = import ./default.nix;

  plotPython = pkgs.python3.withPackages(ps: with ps; [
        # Packages from the stores
        ipython
        numpy
        scipy
        matplotlib
        attrs
        h5py
        pygobject3
        pycairo
        pylint

        # Local packages
        (callPackage qdplot { eqd = eqd; })
    ]
  );
in
  with pkgs;
  {
    production = mkShell {
    buildInputs = [

        # Standard
        which
        gnumake
        git
        cairo
        black

        # Python
        plotPython
      ];
    };
    dev = callPackage qdplot {
      eqd = eqd;
      additionalDevDeps = with python3Packages; [
        pylint
        pytest
      ];
    };
  }

