"""
Specific utils for the electronic quantum dynamics.
"""

import datetime
from pathlib import Path
from typing import Union
from dataclasses import dataclass

from eQD import Calculation

from .utils import mk_figdir


def get_data(date, cname, basepath="./reducedsys"):
    outpath = Path(f"{basepath}/{date}_{cname}/")

    data = []
    for f in outpath.glob("*.h5"):
        inst = Calculation.from_h5(f)
        data.append(inst)

    return outpath, data


def format_tau(tau, inf_thresh: float = 1e7, math_inf: bool = False):
    if tau > inf_thresh:
        return "\\infty" if math_inf else "inf"
    elif 1.0 <= tau < 1e3:
        return f"{tau:.0f}" + (" fs" if not math_inf else "\\,fs")
    else:
        return f"{tau:.2e}" + (" fs" if not math_inf else "\\,fs")


@dataclass
class DataInfo:
    """
    Helper dataclass for storing data info.
    """

    data: list
    cname: str
    date: Union[str, datetime.datetime]
    outpath: Union[Path, str]
    figpath: Union[Path, str]


def get_all_data(
    dates_names,
    basepath: Union[Path, str] = Path("."),
    figbase: Union[Path, str] = Path("."),
):

    basepath = Path(basepath).expanduser()
    figbase = Path(figbase).expanduser()

    data = []
    for date, cname in dates_names:
        outpath, tmpdata = get_data(date, cname, basepath=basepath)
        data.extend(tmpdata)
    else:
        if len(dates_names) == 1:
            figpath = mk_figdir(date, cname, basepath=figbase)
        else:
            date = datetime.datetime.today().strftime("%Y-%m-%d")
            cname = "CombinedPlots"
            figpath = mk_figdir(date, cname, basepath=figbase)

    return DataInfo(data=data, cname=cname, date=date, outpath=outpath, figpath=figpath)
