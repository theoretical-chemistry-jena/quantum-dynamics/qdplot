"""
Small utilities for the analysis of the db files.
"""

def sql_json_qry(prop_name: str, position: int, table_name: str = "data") -> str:
    """
    Make a small JSON query for an SQLite database where the value is only a list
    of values.

    Args:
        prop_name (str): [description]
        position (int): [description]
        table_name (str, optional): [description]. Defaults to "data".

    Returns:
        str: [description]
    """
    pattern = f"json_extract({table_name}.{prop_name}, '$[{position}]')"

    return pattern
