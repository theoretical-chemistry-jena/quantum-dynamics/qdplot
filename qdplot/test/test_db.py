
import sqlite3
from qdplot.db import sql_json_qry

def test_sql_json_qry(idx: int = 1):
    """Basic test case for the sql_json_qry function."""

    db = sqlite3.connect(":memory:")
    db.execute("CREATE TABLE jsont (vals JSON1)")
    db.executemany(
        "INSERT INTO jsont VALUES (?)",
        [("[0.0, 1.0, 2.0, 3.0]", ), ("[0.0, 2.0, 4.0, 6.0]", )],
    )

    # Read as JSON
    rows = list(
        db.execute(
            f"""SELECT {sql_json_qry("vals", idx, table_name="jsont")} FROM jsont"""
        )
    )
    print(rows)

    row = rows[0]
    print(row)

    assert row[0] == float(idx)
    assert len(row) == 1

    # Destroy the database
    del db
