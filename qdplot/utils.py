"""
General utilities for quantum dynamics plotting.
"""

from typing import Any

from pathlib import Path

import numpy as np
import matplotlib as mpl


def mathify(string):
    return r"$\mathregular{{ {} }}$".format(string)


def round_nonfract(a, p=1):
    return float(np.format_float_positional(a, precision=p, fractional=False))


def mk_figdir(date, cname, basepath="figures/NP/Math"):
    figpath = Path(f"{basepath}/{date}_{cname}/")
    if not figpath.exists():
        figpath.mkdir(exist_ok=True, parents=True)

    return figpath


def get_expon(array: np.ndarray):
    return np.floor(np.log10(np.max(array)))


def scale_labels(
    axis: Any, divisor: float, format: str = ".2f", update_locator: bool = True
):
    """
    Change specific x/y tick labels.
    https://numbersmithy.com/how-to-change-axis-tick-labels-in-a-matplotlib-plot/

    Args:
        axis (Axis): .xaxis or .yaxis obj.
        pos (list): indices for labels to change.
        newlabels (list): new labels corresponding to indices in <pos>.
    """

    if update_locator:
        axis.set_major_locator(mpl.ticker.MaxNLocator(nbins=3, prune="both"))

    # Get the respective ticks.
    ticks = axis.get_majorticklocs()
    # print(len(ticks), ticks)

    # Get the default tick formatter
    formatter = axis.get_major_formatter()

    # Modify specific labels
    newlabels = []
    for lbl in ticks:
        labelnum = float(lbl) / divisor
        newlabels.append(f"{labelnum:{format}}")

    # Update the ticks and ticklabels. Order is important here.
    # Need to first get the offset:
    offset = formatter.get_offset()

    # This line suppresses a warning regarding using FixedLocator with FixedFormatter.
    # It's sensible to fix the ticks due to possible issues if the number or location
    # of the ticks change. "Nobody wants that." (FD)
    axis.set_ticks(ticks)

    # Then set the modified labels:
    axis.set_ticklabels(newlabels)
    # In doing so, matplotlib creates a new FixedFormatter and sets it to the xaxis
    # and the new FixedFormatter has no offset. So we need to query the
    # formatter again and re-assign the offset:
    axis.get_major_formatter().set_offset_string(offset)
