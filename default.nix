{ pkgs ? import (import ./nix/sources.nix { }).nixpkgs { }
, eqd ? import (import ./nix/sources.nix { }).eqd { }
, additionalDevDeps ? [ ]
}:

with pkgs.python3Packages; callPackage ./nix/qdplot.nix {
  eqd = eqd;
  additionalDevDeps = additionalDevDeps;
}
